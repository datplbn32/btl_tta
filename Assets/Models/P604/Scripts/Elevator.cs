﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    public float speed;
    public bool isUp, isDown, isClickClose;
    public float min, max, speedIns;
    public bool isRunUp, isRunDown;
    public Transform pos1, pos2;
    public Animator animatorTang1, animatorTang2;
    public static Elevator elevatorIns;
    public int timeAutoClose;

    private void Start()
    {
        isRunUp = isRunDown = false;
        elevatorIns = this;
        min = pos1.position.y;
        max = pos2.position.y;
        speedIns = -speed;
        isUp = isDown = isClickClose = false;
    }
    private void Update()
    {
        if (isUp)
        {
            animatorTang1.enabled = true;
            animatorTang2.enabled = true;
            animatorTang1.SetTrigger("Open");
            isRunUp = true;
        }

        if (isDown)
        {
            animatorTang1.enabled = true;
            animatorTang2.enabled = true;
            animatorTang2.SetTrigger("Open");
            isRunDown = true;
        }

        if (isClickClose)
        {
            if (isRunUp)
            {
                animatorTang1.enabled = true;
                animatorTang1.SetTrigger("Close");

            }
            if (isRunDown)
            {
                animatorTang2.enabled = true;
                animatorTang2.SetTrigger("Close");
            }
        }

        if (transform.position.y >= max - 0.01f && isRunUp)
        {
            isRunUp = false;
            animatorTang2.SetTrigger("Open");
            StartCoroutine(autoClose2());
        }

        if (transform.position.y <= min + 0.01f && isRunDown)
        {
            isRunDown = false;
            animatorTang1.SetTrigger("Open");
            StartCoroutine(autoClose());
        }

        transform.position += new Vector3(0, speedIns * Time.deltaTime, 0);
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, min, max), transform.position.z);
    }

    IEnumerator autoClose()
    {
        yield return new WaitForSeconds(timeAutoClose);
        animatorTang1.SetTrigger("Close");
    }

    IEnumerator autoClose2()
    {
        yield return new WaitForSeconds(timeAutoClose);
        animatorTang2.SetTrigger("Close");
    }

    public void setAvtiveUp() => isUp = true;
    public void setAvtiveDown() => isDown = true;
    public void setAvtiveClose() => isClickClose = true;
}
