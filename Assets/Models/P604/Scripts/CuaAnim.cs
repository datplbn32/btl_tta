using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuaAnim : MonoBehaviour
{
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void setActive() => animator.SetTrigger("Open");
}
