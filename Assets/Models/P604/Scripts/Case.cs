using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Case : MonoBehaviour
{
    public GameObject monitorScreen;
    public bool checkActive;
    // Start is called before the first frame update
    void Start()
    {
        checkActive = false;
        if(monitorScreen != null)
        {
            monitorScreen.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (checkActive && monitorScreen != null)
        {
            monitorScreen.SetActive(true);
        }
    }

    public void On() => checkActive = true;
}
