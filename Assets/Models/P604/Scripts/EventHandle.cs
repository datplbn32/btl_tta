using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandle : MonoBehaviour
{
    public void enableMoveUp()
    {
        if (Elevator.elevatorIns.isRunUp)
        {
            Elevator.elevatorIns.isClickClose = false;  
            Elevator.elevatorIns.speedIns = Elevator.elevatorIns.speed;
            Elevator.elevatorIns.animatorTang1.enabled = false;
        }
    }  
    
    public void enableMoveDown()
    {
        if (Elevator.elevatorIns.isRunDown)
        {
            Elevator.elevatorIns.isClickClose = false;
            Elevator.elevatorIns.speedIns = -Elevator.elevatorIns.speed;
            Elevator.elevatorIns.animatorTang2.enabled = false;
        }
    }

    public void isUpFalse() => Elevator.elevatorIns.isUp = false;
    public void isDownFalse() => Elevator.elevatorIns.isDown = false;
    
}
