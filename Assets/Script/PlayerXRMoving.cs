using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerXRMoving : MonoBehaviour
{
    public KeyCode keyCodeUp;
    public KeyCode keyCodeDown;
    public KeyCode keyCodeleft;
    public KeyCode keyCodeRight;
    public KeyCode keyCodeForward;
    public KeyCode keyCodeBack;

    public float _movingSpeed;

    [SerializeField] private Camera xrCam;

    [SerializeField]
    private GameObject leftHandCtrl;
    [SerializeField]
    private GameObject rightHandCrtl;


    void Update()
    {
        if (Input.GetKey(keyCodeUp))
        {
            this.transform.Translate(_movingSpeed * Time.deltaTime * this.transform.up);
        }    
        if (Input.GetKey(keyCodeDown))
        {
            this.transform.Translate(_movingSpeed * Time.deltaTime * -1 * this.transform.up);
        }
        if (Input.GetKey(keyCodeBack))
        {
            this.transform.Translate(_movingSpeed * Time.deltaTime * -1 * this.transform.forward);
        }
        if (Input.GetKey(keyCodeForward))
        {
            this.transform.Translate(_movingSpeed * Time.deltaTime * 1 * this.transform.forward);
        }
        if (Input.GetKey(keyCodeleft))
        {
            this.transform.Translate(_movingSpeed * Time.deltaTime * -1  * this.transform.right);
        }
        if (Input.GetKey(keyCodeRight))
        {
            this.transform.Translate(_movingSpeed * Time.deltaTime * 1 * this.transform.right );
        }

        this.transform.rotation = xrCam.transform.localRotation;
    }
}
