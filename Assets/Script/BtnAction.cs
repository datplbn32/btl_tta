using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BtnAction : MonoBehaviour
{
    [SerializeField] private Animator animController;
    public bool isActioned = false;
    public string nameParam = "Open";
    public void Action()
    {
        if (isActioned)
        {
            animController.SetBool(nameParam, false);
            isActioned = false;
        }
        else
        {
            animController.SetBool(nameParam, true);
            isActioned = true;
        }
    }
}
