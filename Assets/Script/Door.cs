using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private string animate_param;
    [SerializeField] private bool state;

    [SerializeField] private Animator animator;


    private void Start()
    {
        animator = GetComponent<Animator>();    
    }


    public void ChangeState()
    {
        //if (Input.GetKey(KeyCode.O)) {
            state = !state;
            animator.SetBool(animate_param, state);
        //}
    }
}
