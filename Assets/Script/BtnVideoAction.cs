using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class BtnVideoAction : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoAction;


    public void OpenVideo()
    {
        videoAction.enabled = true;
    }
    
    public void CloseVideo()
    {
        videoAction.enabled = false;
    }
}
