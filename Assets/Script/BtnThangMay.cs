using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BtnThangMay : MonoBehaviour
{

   [SerializeField] private Animator animController;
   [SerializeField] private Transform thangMay;
   [SerializeField] private Transform thangMayT6;
   [SerializeField] private Transform cameraRig;
   [SerializeField] private float yAxis;
   [SerializeField] private float time;
   public void ActThangMay()
   {
      animController.SetInteger("Act", 1);
      StartCoroutine(CloseDoor());
   }

   public IEnumerator CloseDoor()
   {
      yield return new WaitForSeconds(7f);
      animController.SetInteger("Act", 2);
   }

   public void MoveThangMay()
   {
      cameraRig.SetParent(thangMay);
      thangMay.DOMove(thangMayT6.position, time).SetEase(Ease.Linear).OnComplete(() =>
      {
         thangMayT6.gameObject.SetActive(false);
         cameraRig.SetParent(null);
         animController.SetInteger("Act", 1);
         StartCoroutine(CloseDoor());
      });
   }
}
