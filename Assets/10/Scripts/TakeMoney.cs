using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeMoney : MonoBehaviour
{
    [SerializeField] private List<GameObject> listMoney;
    [SerializeField] private Animator paperAnim;
    public void OpenObj()
    {
        foreach (var obj in listMoney)
        {
            obj.SetActive(true);
        }
    }
    
    public void CloseObj()
    {
        foreach (var obj in listMoney)
        {
            obj.SetActive(false);
            paperAnim.enabled = true;
        }
        
        //dotween
    }
}
