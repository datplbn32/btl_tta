using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionDoor : MonoBehaviour
{
    [SerializeField] private GameObject btn;
    [SerializeField] private Animator animDoor;

    public void OpenDoor()
    {
        btn.SetActive(false);
        animDoor.enabled = true;
    }
}
