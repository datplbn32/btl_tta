﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushToCar : MonoBehaviour
{
    private bool isIn;
    [SerializeField] private Transform cameraRig;
    [SerializeField] public Transform avatarObj;
    public Vector3 posPlayer = new Vector3(11f, 18f, 17f);
    public GameObject carParent;
    public BoxCollider cols;
    public float height;
    private void Start()
    {
        height = transform.position.y;
    }

    public void PushIn()
    {
        var transform1 = transform;
        cameraRig.parent = carParent.transform;
        cameraRig.localPosition = posPlayer;
        isIn = true;
        cols.enabled = false;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Car"))
        {
            carParent = other.gameObject;
            PushIn();
            carParent.GetComponent<MoveController>().isMove = true;
        }
    }

    public void PushOut()
    {
        isIn = false;
        cameraRig.parent = null;
        carParent.GetComponent<MoveController>().isMove = false;
        cameraRig.position += new Vector3(5, height, 0);
        cols.enabled = true;
    }
    
    void Update()
    {
        if (isIn)
        {
            avatarObj.localPosition = Vector3.zero;
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            PushOut();
        }
    }
}
